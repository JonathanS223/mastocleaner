#!/bin/bash

#
# MASTO CLEANER v0.01
#
# by Jonathan Snyder (jtworld.net)
#
# (c) 2022 by Jonathan Snyder. Released under the MIT license. 

#this script is designed to help automate and clean a mastodon single user from all the chaff that could be pulled in from the fediverse. 

# CONFIG - YOU CAN MODIFY BEHIND THIS LINE IF YOU HAVE CHANGED DEFAULT INSTALLATION

logName=MastoCleaner.log #You can change the name of the log if you want. 
liveLocation="/home/mastodon/live" #This is the default location of a Mastodon install. Change if you did something different. 
days=2 #How many days back do you want to have the cleaner keep?

# DO NOTE CHANGE AFTER THIS LINE UNLESS YOU KNOW WHAT YOU ARE DOING.

theTime=$(date)

#check to see if a log file has been created. 
if [ -f $logName ]; then
    echo "--STARTING PROCESS--">>$logName
    echo "Time & Date is: $theTime">>$logName
    echo "--">>$logName
else
    #create the log file if it doesn't exist and give it permissions. 
    echo "***MastoCleaner Log 0.01v***">>$logName
    echo "">>$logName
    echo "--STARTING PROCESS--">>$logName
    echo "Time & Date is: $date">>$logName
    echo "--">>$logName
fi

#Time to go to the correct folder. 
cd $liveLocation

tootVersion=$(RAILS_ENV=production bin/tootctl --version)
echo $tootVersion>>$logName

#time to purge the cache first 

cacheStatus=$(RAILS_ENV=production bin/tootctl cache clear)
wait
status=$?
if [ $status -ne 0 ]; then
    echo "There has been an error with tootctl cache clear. Please run the command separate from script to see what the error is.">>$logName
else
    echo "-Cache Clearing has been completed successfully.">>$logName
fi

mediaClean=$(RAILS_ENV=production bin/tootctl media remove --days $days)
wait
status=$?

if [ $status -ne 0 ]; then
    echo "There has been an error with tootctl Media remove. Please run the command separate from script to see what the error is.">>$logName
else 
    echo "-Media Removal has been completed successfully.">>$logName
fi

#Uncomment this section if you want to clean statuses but NOTE. It is computaionally heavy. URL: https://docs.joinmastodon.org/admin/tootctl/#statuses-remove

#statusRemoval=$(RAILS_ENV=production bin/tootctl statuses remove --days $days)
#wait
#status=$?

#if [ $status -ne 0 ]; then
#    echo "There has been an error with tootctl statuses remove. Please run the command separate from script to see what the error is.">>$logName
#else
#    echo "-Status removing has been completed.">>$logName

echo "--COMPLETE--">>$logName
exit 0