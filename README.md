# MastoCleaner

## Introduction
This shell script is designed with the intention of helping single user instances of Mastodon social automate some of the cleaning and maintenance of their server.

## Requirements
[] Ubuntu 20.04 or later<br />
[] Mastodon 4.0.2<br />
[] crontab<br />
[] Bash 5.0.17 or later<br />
[] Root access to server<br />

## Installation

### Step 1 - Installation and permissions
This program is designed to be dropped directly into the Mastodon home folder outside of the live folder that is installed. Use your favorite FTP or file putter to place it on your remote server. Once there, grant the script the ability to run. 

#### Install Bash on system (if not already installed)
Bash is fundemental to get this to work so make sure it is installed. 

```
apt-get update && apt-get install bash -y
systemctl status bash
```

#### Install Crontab (if not already installed)
Execute the following commands as root or a user with sudo permissions. (You will need to put sudo in front of each if you're running it from a sudo user.)

```
apt-get install cron -y
systemctl status cron
```

#### Set MastoCleaner permissions

```
chmod +x MastoCleaner.sh
```

### Step 2 - Setup Crontab to run the script. 
It's time to create a crontab. You're going to do it under the mastodon user that you created when initially installed mastodon. If you used a different name, substitute mastodon with that name.

```
sudo su - mastodon
crontab -e
```

Add the following line at the next available below any you might already have. 

```
0 0 * * SAT /home/mastodon/mastocleaner/MastoCleaner.sh >> /home/mastodon/mastocleaner/cronlog.log
```

This cron will run on every Saturday at midnight. Please note, this script may take awhile to run depending on how large everything is. You want to budget enough time around your server's 
lowest usage so that you are not burdening it while you and users might be using it. 

## (OPTIONAL) Don\'t wait for the CRON
If you want to launch the script immediately to clean it out, you can do so. NOTE: THERE WILL BE NO MESSAGES AS EVERYTHING IS PUT IN A LOG IN THE SAME FOLDER. TO SEE WHAT HAPPENED, YOU\'LL NEED TO ACCESS THE LOG IN THE SAME FOLDER.

```
cd mastocleaner
./mastoCleaner.sh
```

## License
This script is released under the MIT license.


## Future of the script
This script will be continually developed as I discover more ways of cleaner and maintaining my mastodon instance. I plan to put some postgresql cleaning and other things as I learn more about how those actually operate.